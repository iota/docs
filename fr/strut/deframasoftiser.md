# Déframasoftiser Framaslides

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Pour exporter vos présentations (une à une), vous devez&nbsp;:

### Méthode 1

  1. vous rendre sur la page **[Mes présentations](https://framaslides.org/presentations)**
  * cliquer sur l'icône <i class="fa fa-cog" aria-hidden="true"></i> sur la ligne de la présentation à exporter
  * cliquer sur le bouton **JSON** dans la section **Télécharger la présentation**
  * télécharger le fichier export `nom-du-projet.json` sur votre ordinateur

### Méthode 2

Depuis la page de modification de votre présentation, vous devez&nbsp;:

  1. cliquer sur **Framaslides <i class="fa fa-caret-down" aria-hidden="true"></i>**
  * cliquer sur **Exporter…**
  * cliquer sur l'icône <i class="fa fa-download" aria-hidden="true"></i> dans l'onglet **JSON**
  * télécharger le fichier export `nom-du-projet.json` sur votre ordinateur

## Importer

Pour importer vos projets dans une nouvelle instance utilisant le logiciel **Strut**, vous devez&nbsp;:

  1. créer une nouvelle présentation
  * cliquer sur le nom du site dans le coin supérieur gauche
  * cliquer sur **Importer…**
  * récupérer le fichier `JSON` téléchargé ([voir ci-dessus](#exporter))

Votre présentation est désormais importée : vous pouvez l'enregistrer.

## Supprimer votre compte Framaslides

Pour supprimer votre compte vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-cog" aria-hidden="true"></i> pour accéder à [vos paramètres](https://framaslides.org/config)
  * cliquer sur l'onglet **Information utilisateur**
  * cliquer sur le bouton **Supprimer mon compte**
  * cliquer sur **OK** à la demande de confirmation
