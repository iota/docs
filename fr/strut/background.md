# Arrière plan

Les diapositives peuvent avoir deux types d'arrière plans : un arrière plan uniquement sur la diapositive elle-même et un arrière plan sur l'écran tout entier.

Il est possible de définir l'arrière-plan pour la diapositive courante ou bien toutes les diapositives.

L'arrière plan peut-être une des couleurs prédéfinies, un fond transparent ou une couleur choisie. Il est également possible de téléverser une image pour en faire le fond de la diapositive.
