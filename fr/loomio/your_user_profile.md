# Votre profil utilisateur

Votre [page de profil](https://framavox.org/profile) vous permet de paramétrer votre identité Framavox. Pour visiter votre page de profil&nbsp;:

  1. cliquez sur <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquez sur votre nom d’utilisateur
  * sélectionnez **Modifier mon profil**

## Mettre à jour vos paramètres personnels

Vous pouvez mettre à jour le nom, l'identifiant utilisateur, l‎‎’adresse de courriel, une présentation, un lieu et les paramètres de langue associés à votre compte en renseignant les champs correspondants sur votre page de profil, et en cliquant sur **Mettre à jour le profil**.

### Afficher une photo pour votre profil

Quand vous créez un compte Framavox, vos initiales vont être l‎‎’image par défaut associée à votre profil. Vous pouvez mettre en ligne une photo de profil en cliquant sur **CHANGER DE PHOTO** dans [votre page de profil](https://framavox.org/profile), et en cliquant sur **Mettre à jour le profil**.

### Paramètres de langue

Framavox est accessible dans de nombreuses langues. Il va détecter automatiquement la langue de votre navigateur, mais vous pouvez choisir manuellement votre langue préférée en allant sur votre [page de profil](https://framavox.org/profile) et en sélectionnant votre langue depuis le menu déroulant **Langue**, et en cliquant sur **Mettre à jour le profil**.

### Nom d‎‎’utilisateur

Votre nom d‎‎’utilisateur est le nom que les autres participants vont utiliser pour vous mentionner ([@mention](comments.html#-mentioning-group-members)) dans les commentaires. Vous pouvez changer votre nom d‎‎’utilisateur en renseignant le champ **Identifiant utilisateur** sur votre [page de profil](https://framavox.org/profile) et en cliquant sur **Mettre à jour le profil**.

## Changer votre mot de passe

Si vous êtes connecté à Framavox, vous pouvez changer votre mot de passe en allant sur votre [page de profil](https://framavox.org/profile) et en cliquant sur **Réinitialiser mon mot de passe**. Vous devrez alors saisir votre nouveau mot de passe deux fois puis cliquer sur le bouton **DÉFINIR UN MOT DE PASSE**.

Si vous êtes déconnecté, vous devez cliquer sur **RECEVOIR UN COURRIEL DE CONNEXION** à l'étape de demande du mot de passe lors de la connexion.

## Désactiver votre compte

Si vous n‎‎’êtes pas l‎‎’unique coordinateur d‎‎’un groupe vous pouvez désactiver votre compte en allant sur votre [page de profil](https://framavox.org/profile) et en cliquant sur **Compte désactivé** puis sur **OUI, JE VEUX DÉSACTIVER**. Si vous êtes le seul coordinateur d‎‎’un groupe, vous ne pourrez pas désactiver votre compte tant que vous n‎‎’aurez pas désigné un autre coordinateur de groupe, ou archivé le groupe.

Une fois que votre compte a été désactivé&nbsp;:

* Vous ne serez plus enregistré comme membre d’aucun groupe
* Les commentaires, les décisions et les discussions que vous avez fait resteront mais votre nom en sera retiré
* Vous ne recevrez plus de courriels de notification
* Vous pouvez réactiver votre compte ultérieurement si vous le souhaitez.

## Réactiver un compte

Pour réactiver un compte désactivé, [contactez nous](https://contact.framasoft.org).

## Supprimer un compte

Vous pouvez supprimer votre compte définitivement en allant dans [votre profil](https://framavox.org/profile) et en cliquant sur **Supprimer le compte utilisateur**.

<div class="alert-warning alert">
  <p>Ceci ne peut pas être annulé. Si vous continuez :</p>
  <ul>
    <li>Votre compte d'utilisateur, y compris votre nom, votre adresse e-mail et d'autres données d'identification personnelle seront supprimés de nos systèmes.</li>
    <li>Vous ne serez plus enregistré comme membre d’aucun groupe</li>
    <li>Les commentaires, les décisions et les discussions que vous avez fait resteront mais votre nom en sera retiré</li>
  </ul>
</div>
