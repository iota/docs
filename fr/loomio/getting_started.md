# Premiers pas

Il existe plusieurs façons de s‎‎’initier à Loomio. Voici quelques informations sur chaque point d‎‎’entrée disponible&nbsp;:

## Créer un nouveau groupe depuis la page d‎‎’accueil

Vous pouvez créer un nouveau groupe depuis la page d‎‎’accueil de Loomio en cliquant sur le bouton **Essayer Loomio**. Saisissez votre nom, une adresse électronique et le nom du groupe dans le formulaire [**Créer un nouveau groupe**](https://www.loomio.org/start_group). Vous recevrez par mail un lien qui vous conduira vers le nouveau groupe que vous venez de créer.

## Vous êtes invité dans un groupe Loomio

Lorsque vous recevez une invitation à rejoindre un groupe Loomio existant, vous recevez un lien d’invitation unique par courriel.

Si c’est votre première utilisation de Loomio, il vous sera demandé de créer un compte. Si vous possédez déjà un compte, vous serez amené directement à ce groupe une fois authentifié.

## Single sign-on

<img class="screenshot" alt="Log in page with single sign-on options" src="images/log_in_page.png" />

Loomio permet de vous enregistrer avec Facebook, Google, Twitter ou Persona. Si vous n’avez pas de compte Loomio, vous pouvez vous enregistrer en utilisant l’un de ces services tiers en cliquant sur le bouton correspondant sur la [page de **connexion**](http://loomio.org/sign_in). Si vous êtes déjà authentifié sur Google, Facebook, Twitter ou Persona, nous lierons cette méthode d’authentification à votre compte Loomio. Sinon, il vous sera demandé de vous y authentifier. Une fois que Loomio reconnaît votre compte tiers existant, suivez le dialogue pour créer votre compte Loomio.

Vous pouvez aussi associer votre compte Google, Facebook, Twitter ou Persona à un compte Loomio existant. Assurez-vous d’être déconnecté, visitez [la page de **connexion**](https://www.loomio.org/users/sign_in) et choisissez votre plateforme préférée. Loomio vous demandera si vous souhaitez créer un nouveau compte avec votre compte existant sur la plateforme, ou le lier à un compte Loomio existant.
