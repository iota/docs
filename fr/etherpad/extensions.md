# Modifications apportées à Framapad

Au cours des mois d'avril et mai 2016, nous avons apporté plusieurs modifications
à l'éditeur collaboratif [Framapad](https://framapad.org).
Cette page vous présente ces modifications ainsi que la procédure à suivre pour les activer/désactiver.

Pour profiter de ces nouvelles options, vous aurez éventuellement besoin d'accéder à vos paramètres :

![](images/Selection_052.png)

## Réactivation des couleurs d'identification des auteurs.

Lorsque différents auteurs écrivent, chacun dispose de la couleur de surlignage de son choix.
Cependant, ce comportement peut parfois être gênant (un très grand nombre d'auteurs peut gêner la lisibilité, par exemple).

Au lieu d' “Effacer les couleurs identifiant les auteurs” (ce qui les supprime définitivement pour **tout le monde**),
il est possible de désactiver cette fonctionnalité simplement en allant dans vos paramètres.
Ainsi, les couleurs seront cachées sur votre navigateur mais disponibles pour les autres utilisateurs du pad.

Pour ce faire, vous devez&nbsp;:

  1. cliquer sur <i class="fa fa-cog" aria-hidden="true"></i>
  * (dé)cocher la case **Couleurs d'identification**

**Avant**&nbsp;: ![](images/Selection_050.png)

**Après**&nbsp;: ![](images/Selection_051.png)

## Affichage du nom des auteurs au survol

Si vous avez désactivé l'option « Couleurs d'identification »,
vous apprécierez sûrement cette option : en survolant un texte,
le nom de l'auteur (s'il en a saisi un) apparaîtra pendant quelques secondes au-dessus du curseur.

Pour activer/désactiver cette possibilité&nbsp;:

  1. cliquez sur <i class="fa fa-cog" aria-hidden="true"></i>
  * cliquez sur **Show Author on Hover**

![](images/Selection_061.png)

## Table des matières

Il est maintenant possible d'afficher une table des matières à droite de votre texte.

Les éléments de cette liste sont générés automatiquement à partir des **niveaux de titres de votre pad**

Un clic sur le titre dans la table des matières vous amènera directement à la section concernée.

Pour l'activer&nbsp;:

  1. cliquez sur <i class="fa fa-cog" aria-hidden="true"></i>
  * cliquez sur **Afficher la table des matières**

**Avant**&nbsp;: ![](images/Selection_055.png)

**Après**&nbsp;: ![](images/Selection_056.png)


## Mode « page en largeur pleine »

Par défaut, la page est «&nbsp;réduite&nbsp;» pour un confort de lecture accru. Pour avoir la page en largeur pleine vous devez&nbsp;:

  1. cliquer sur <i class="fa fa-cog" aria-hidden="true"></i>
  * cliquer sur **Page en largeur pleine**
