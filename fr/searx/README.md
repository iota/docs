# Framabee

<div class="alert-warning alert">
<p>Framabee a définitivement fermé en Octobre 2019 ; Plus d'informations sur <a href="https://framablog.org/2019/09/24/deframasoftisons-internet/">le Framablog</a>.</p>
<p>Ce service n’existe plus ici mais <a href="https://alt.framasoft.org/fr/framabee">on vous indique où le retrouver</a> !</p>
</div>

Framabee est un métamoteur de recherche regroupant les résultats d'autres moteurs de recherche mais sans conserver d'informations sur les utilisateurs.

Framabee ne vous trace pas, ne partage aucune donnée avec un tiers et ne peut pas être utilisé pour vous compromettre.

## Pour aller plus loin

  * [Prise en mains](prise-en-mains.html)
  * ~~[Utiliser Framabee](https://framabee.org/)~~
  * Un service proposé dans le cadre de la campagne [Dégooglisons Internet](https://degooglisons-internet.org)
