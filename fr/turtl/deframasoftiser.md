# Déframasoftiser Framanotes

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

Il n'existe pas d'équivalent **web** à Framanotes mais une application. Framanotes repose sur un logiciel (turtl) qui n'est, normalement, utilisé que par des **applications** : https://turtlapp.com/download/ - nous avions décidé de rendre son utilisation possible avec un navigateur.

Si vous utilisez le site Framanotes via votre navigateur web, vous allez devoir migrer vos données vers une application&nbsp;: https://turtlapp.com/download/

<p class="alert-info alert">Dans ce tutoriel, la migration depuis Framanotes se fait <b>vers</b> <code>https://apiv3.turtlapp.com/</code> (le site de l'application)&nbsp;; la limite de stockage pour la version gratuite est de <b>50Mo</b> : voir <a href="https://turtlapp.com/premium/">https://turtlapp.com/premium/</a>.</p>

## Je n'utilise que le site

<p class="alert-info alert">Cette méthode fonctionne aussi si vous n'avez pas migré vos données vers https://apiv3.framanotes.org/ (pour, par exemple, continuer à utiliser le site)&nbsp; cela signifie que vous utilisez une application <b>antérieure à la version 0.7</b>.</p>

Pour récupérer vos données depuis le site https://mes.framanotes.org/ vous devez installer une application parmi celles disponibles sur le site https://turtlapp.com/download/

<p class="alert-info alert">Pour ce tutoriel nous allons utiliser l'application pour Android</p>

Sur la première fenêtre au démarrage de l'application, vous devez&nbsp;:

  * 1) entrer votre identifiant
  * 2) entrer votre mot de passe Framanotes
  * 3) cliquer sur le bouton **Connexion** pour pouvoir créer un nouveau compte

![image de la page de connexion turtl](images/framanotes-deframatisons-1.jpg)

  * 4) entrer votre nouvel identifiant (qui sera une adresse mail, cette fois-ci)
  * 5) entrer votre mot de passe
  * 6) confirmer votre mot de passe

![](images/framanotes-deframatisons-3.jpg)

  * 7) entrer l'adresse de l'ancien serveur Framanotes `https://api.framanotes.org/`
  * 8) entrer le nouveau serveur (par défaut, celui de l'application `https://apiv3.turtlapp.com`)

![](images/framanotes-deframatisons-4_2.jpg)

Vos données seront alors synchronisées sur **le nouveau serveur** (`https://apiv3.turtlapp.com` dans l'exemple ci-dessus) mais ne le seront plus sur https://mes.framanotes.org/.

Une fois vos données sur le nouveau serveur vous pouvez [supprimer celles de Framanotes](https://contact.framasoft.org/fr/faq#notes_suppression_compte). Attention à bien vérifier que les données sont sur le nouveau serveur : **nous ne pourrons récupérer vos données supprimées**.

## J'utilise l'application

Si vous utilisez l'application turtl avec `https://apiv3.framanotes.org/` vous pouvez migrer facilement vos données vers un autre serveur (comme `https://apiv3.turtlapp.com` par exemple). Pour ce faire, vous devez&nbsp;:

![](images/framanotes_migration_app.gif)

  1. cliquer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
  * cliquer sur **Paramètres**
  * cliquer sur **Import & export**
  * cliquer sur le bouton **Export**
