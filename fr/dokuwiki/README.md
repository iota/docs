# Framasite

[Framasite Wiki](https://frama.site/) est un service libre de création de wikis.

Les Framawiki sont des sites collaboratifs et multi-pages, demandant un poil de connaissances mais restant assez aisés d'usage.

Le service repose sur le logiciel libre [DokuWiki](https://www.dokuwiki.org/start?id=fr:dokuwiki).

---

## Pour aller plus loin&nbsp;:

- Utiliser [Framasite](https://frama.site/)
- [Site de démonstration](https://valvin.frama.wiki/quickstart)
- [Déframasoftiser Framasite wiki](deframasoftiser.md)
- [Astuces](astuces.md)
- [Le site officiel de DokuWiki](https://www.dokuwiki.org/start?id=fr:dokuwiki) (pensez à [les soutenir](https://www.dokuwiki.org/fr:donate) !)
- Un service proposé dans le cadre de la campagne [contributopia](https://contributopia.org/)
