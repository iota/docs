# Framemo

<b class="violet">Fra</b><b class="vert">memo</b> est un service en ligne libre et minimaliste qui permet d’éditer et d’organiser collaborativement des idées sous forme de notes

  1. Créez un tableau en choisissant son nom (attention, ne choisissez pas un nom trop générique, il risque d’être choisi par d’autres personnes)
  - Votre tableau est sauvegardé en temps réel, il vous suffit de partager son adresse web.
  - Créez une note en cliquant sur le (+) à gauche (cliquez plusieurs fois pour en changer la couleur), puis déplacez-la sur le tableau. Double-cliquez pour la modifier.
  - Déplacez-les en cliquant dessus puis en les faisant glisser vers leur destination
  - Modifiez-les en double-cliquant dessus
  - Les gommettes (en bas à droite) peuvent être placées sur les notes, pour représenter —&nbsp;par exemple&nbsp;— une personne, une priorité, un degré d'importance ou d'urgence…
  - Organisez votre tableau avec des colonnes&nbsp;! Pour ajouter ou supprimer une colonne, cliquez sur les (+) ou (-) qui apparaissent en plaçant votre souris à droite du tableau. Double-cliquez sur le nom d’une colonne pour la renommer.

<b class="violet">Fra</b><b class="vert">memo</b> repose sur le logiciel [Scrumblr](https://github.com/aliasaria/scrumblr) développé par Ali Asaria.

---

### Pour aller plus loin&nbsp;:

  * [Déframasoftiser Internet](deframasoftiser.html)
  * Essayer [Framemo](https://framemo.org/)
  * Le [site officiel](http://scrumblr.ca/) (à soutenir&nbsp;!)
  * [Participer au code](https://github.com/aliasaria/scrumblr) de Scrumblr
  * Installer [Scrumblr sur vos serveurs](http://framacloud.org/cultiver-son-jardin/installation-de-scrumblr/)
  * [Dégooglisons Internet](https://degooglisons-internet.org/)
  * [Soutenir Framasoft](https://soutenir.framasoft.org/)
  * [Foire Aux Questions](https://contact.framasoft.org/foire-aux-questions/#framemo)
