# Modération

---

  * [La charte de modération](https://framasoft.org/fr/moderation/)
  * [Framasphère / diaspora*](moderation_diaspora.md)
  * [Framapiaf / Mastodon](moderation_mastodon.md)
  * [Framacolibri / Discourse](moderation_discourse.md)
