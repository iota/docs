# Exemple d’utilisation de Framalistes

## La famille Dupuis-Morizeau s’ouvre une Framaliste&nbsp;!


Fanny Dupuis-Morizeau a un gros problème : avec son épouse, Cécile, et
leurs enfants, elles ont décidé de faire un tour du monde dans leur
bateau. Bon, ça, c’est plutôt plaisant. Mais entre la mamie accro à son
iPad, les cousins à fond sur GNU/Linux, et la connexion approximative
qu’elles auront en mer, le plus simple pour rester en contact avec les
nombreux membres de leur famille recomposée reste l’email. Qu’à cela ne
tienne, Fanny se dit qu’elle va ouvrir une Framaliste pour toute sa
(grande) famille. Elle se rend donc sur
[Framalistes.org](https://framalistes.org) et se crée un compte.
Jusqu’ici tout va bien :

1.  Elle entre son adresse email
2.  Reçoit l’email d’activation
3.  Clique sur le lien reçu pour décider de son mot de passe
4.  Puis elle saisit ses informations personnelles
5.  …sans oublier de cliquer sur Valider&nbsp;! (elle connaît les [Conditions
    Générales d’Utilisation](https://framasoft.org/nav/html/cgu.html)
    des services Framasoft, et ça lui convient bien.)

![entrer son email](images/framalistes-01-email.png "Entrer son email pour recevoir un lien d’activation")

![Définir son mot de passe](images/framalistes-02-mdp.png "Définir son mot de passe")

![Entrer ses infos personnelles](images/framalistes-03-infos-perso.png "Entrer ses infos personnelles")

Vient le
moment de créer la liste. Elle clique sur le bouton «&nbsp;créer une liste&nbsp;»
dans la barre en haut à droite, et doit choisir le nom, donc ce qui se
trouvera avant le «&nbsp;@framalistes.org&nbsp;». Ayant un nom composé, elle
connaît le piège que représentent les traits d’union, par exemple quand
on doit donner un email par téléphone. Elle ne supporte plus d’entendre
parler du «&nbsp;tiret-du-six&nbsp;», ça la rendrait méchante. Elle choisit donc
la stratégie du «&nbsp;tout-attaché-en-minuscules-et-sans-accents&nbsp;» qui a
fait ses preuves&nbsp;!


![creation A](images/framalistes-04-creation-A-1.png)

Par défaut, elle choisit une liste de type confidentielle, mais elle se
dit qu’elle pourra affiner les paramétrages un peu plus tard.

![creation B](images/framalistes-04-creation-B.png)

Enfin, elle soigne son objet et la description de sa liste, car elle se
doute que ce sera réutilisé par le logiciel (elle a raison, la
description est le texte qui s’inscrit par défaut dans les emails
accueillant les nouveaux abonnés).

![creation C](images/framalistes-04-creation-C.png)

Voilà, la liste est créée&nbsp;! Tiens, Fanny lit qu’elle n’y est pas
abonnée. Au départ elle trouve cela étrange, puis elle réalise que cela
peut lui permettre de créer une liste de diffusion pour les camarades de
classe de sa fille sans qu’elle en reçoive les messages… plutôt pratique
! Fanny s’abonne donc d’un clic à sa liste toute neuve, et note dans un
coin les liens pour s’abonner et se désabonner (elle les enverra plus
tard à la famille)

![post creation](images/framalistes-05-post-creation.png)

Il est temps de paramétrer cette liste. Fanny se rend sur l’accueil de
la liste, et voit que ses options d’administration y apparaissent. Bien
! Elle décide de modifier le message de bienvenu aux nouveaux inscrits à
la liste, de vérifier qui peut y envoyer des messages, et de faire un
joli en-tête pour les emails.

![accueil](images/framalistes-06-accueil-1.png)

![Editer l’email de bienvenue](images/framalistes-07-A-editer-meessage.png "Editer l’email de bienvenue")

![Qui peut envoyer des messages&nbsp;?](images/framalistes-07-B-envois-1.png "Qui peut envoyer des messages&nbsp;?")

![Une nouvelle en-tête pour les messages de la liste](images/framalistes-07-C-en-tete-1.png "Une nouvelle en-tête pour les messages de la liste")

Fanny pourrait
continuer longtemps à s’amuser à paramétrer sa liste, mais elle veut
tester un truc… Elle envoie par email la page d’accueil de sa liste à
son épouse, Cécile : <https://framalistes.org/sympa/info/dupuismorizeau>.

Cécile n’étant pas propriétaire de la liste, elle n’en voit pas les
options d’administration. De fait, elle a simplement une page d’accueil
descriptive et des liens très pratiques pour s’abonner (ou se
désabonner), ce qu’elle fait de ce pas&nbsp;!

![infos](images/framalistes-08-infos.png)

Bon, c’est décidé, Fanny va envoyer ce lien à toute la grande famille
pour que chacun-e puisse enfin s’y inscrire. Très vite, elle propose à
son cousin Solal (un autre doué du clavier) d’en devenir le
co-propriétaire&nbsp;!

![ajout proprio](images/framalistes-09-ajout-proprio.png)

Les échanges vont bon train sur la liste. David, qui vient d’entrer dans
la famille Dupuis-Morizeau par amour, accède à la liste&nbsp;! Pas de souci,
il rattrape son retard dans les conversations en consultant les
archives…

![archives](images/framalistes-10-archives.png)

Pas besoin d’être un-e Dupuis-Morizeau pour tester Framalistes, ni même
d’avoir une famille élargie et recomposée : que vous soyez en
association, en syndicat, dans un collectif artistique, un club de
sport, une institution ou une PME… Ou que vous vouliez juste vous
rassembler par centre d’intérêt, l’outil s’adaptera à vos besoins&nbsp;! Chez
Framasoft, cela fait deux ans que nous travaillons à distance des 6
coins de l’hexagone (et au-delà) avec des listes de diffusion comme
outil principal ^^&nbsp;! Désormais, C’est à vous de le tester, le partager
et l’adopter.



