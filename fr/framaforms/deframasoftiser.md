# Déframasoftiser Framaforms

Afin de [déframasoftiser Internet](https://framablog.org/2019/09/24/deframasoftisons-internet/) voici comment exporter vos données depuis nos services.

## Exporter

Vous pouvez exporter vos formulaires pour les importer dans une autre instance utilisant le logiciel utilisé par Framaforms ([drupal](https://www.drupal.org/) avec le module [webform](https://www.drupal.org/project/webform)). Pour ce faire vous devez&nbsp;:

  * cliquer sur l'onglet **Formulaire**
  * cliquer sur **Exporter**

La structure du formulaire et les conditions seront exportées.

Pour exporter les soumissions de votre formulaire vous devez&nbsp;:

  * cliquer sur l'onglet **Résultats**
  * cliquer sur **Téléchargement**
  * **Facultatif** : modifier les préférences de téléchargement des résultats
  * cliquer sur le bouton **Téléchargement**
