# FAQ
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

## Compte Framagenda

#### Pourquoi ne puis-je pas téléverser de fichiers sur Framagenda ?
Framagenda n'est pas un service d'hébergement de fichiers (voir [Framadrive](https://framadrive.org) à la place). Il est possible de téléverser de très petits fichiers (nous avons fixé la limite à 5MiB) mais cela est seulement «&nbsp;toléré&nbsp;» si vous voulez ajouter une petite pièce jointe à un événement.

#### Je veux supprimer mon compte !

Pour supprimer votre compte, vous devez vous rendre dans [vos paramètres](https://framagenda.org/index.php/settings/personal#drop-account) puis cocher la case **Cochez cette case pour confirmer la demande de suppression** et enfin cliquer sur **Supprimer mon compte**.

#### Je veux changer mon nom d'utilisateur

Cela n'est pas possible dans l'application NextCloud. Vous pouvez cependant modifier le nom affiché dans l'application dans vos paramètres personnels. Si une modification du nom d'utilisateur est importante pour vous, il vous faut recréer un compte et fermer votre actuel, après avoir exporté et importé vos données.

#### Je n'arrive pas à changer mon mot de passe

Si le formulaire de changement de mot de passe dans les paramètres personnels ne fonctionne pas, vous pouvez demander à réinitialiser votre mot de passe en cliquant sur le lien **Mot de passe oublié ?** sous le formulaire de connexion de la page https://framagenda.org/login puis en mettant soit votre identifiant soit l'adresse mail qui lui est associé pour recevoir un mail de réinitialisation.

#### Je n'arrive pas à me logger avec Chrome, Opera, ou le navigateur de base sur Android

Il s'agit d'un problème connu, mais difficile à identifier. Toujours est-il que l'authentification fonctionne correctement avec Firefox sur Android.

## Fonctionnalités

#### Je ne vois pas mes événements dans Framagenda ou sur mon client !

Vérifiez que votre agenda ne se trouve pas dans l'état « désactivé ». Dans Framagenda, cliquez sur le nom de l'agenda. Si une pastille de couleur s'affiche à gauche du nom de l'agenda, c'est que celui-ci est activé.

#### Les rappels ne fonctionnent pas !

Bien qu'ils apparaissent dans l'interface, les rappels par mail, son ou encore fenêtre d'alerte ne fonctionnent pas encore dans Framagenda. En revanche, vous devriez avoir des notifications sur votre appareil mobile si vous synchronisez vos agendas dessus.

#### Je veux copier un événement vers un autre agenda

Il n'a pas encore cette fonctionnalité dans Framagenda, mais vous pouvez exporter l'événement au format .ics, puis l'importer dans l'agenda que vous voulez.

#### Comment puis-je transférer mes données de Google Agenda à Framagenda ?

Vous devez vous rendre dans les paramètres de Google Agenda, puis sur l'onglet **Agendas**, vous avez un lien **Exporter les agendas**.

Cliquer dessus vous donne accès à un fichier zip contenant vos agendas au format .ics. Vous devrez dézipper ce fichier zip pour récupérer chaque fichier correspondant à chaque agenda, pour ensuite les importer dans Framagenda.

Dans Framagenda, l'import se passe dans les paramètres (en bas à gauche de l'interface), en cliquant sur **Importer un agenda** puis en choisissant un fichier .ics. Une fois le fichier importé, Framagenda va vous demander si vous voulez importer les événements dans un calendrier existant ou dans un nouvel agenda.

Répétez l'opération pour autant d'agendas que vous aviez sur Google Agenda.

<div class="alert-warning alert">
<p>Il semblerait que Google ne rafraîchisse les souscriptions que toutes les 12 heures au minimum. La seule « solution » semble être de tricher en enlevant l'agenda et en le remettant avec une URL un peu différente (<a href="https://helpme.teamsnap.com/article/315-team-schedule-is-not-updating-in-google-calendar">voir en anglais</a>).</p>

<p>Ainsi pour une URL du type&nbsp;:</p>

<code>
webcal://framagenda.org/remote.php/dav/public-calendars/votrecode?export
</code>

<p>On pourra mettre à la place&nbsp;:</p>

<code>
webcal://framagenda.org/remote.php/dav/public-calendars/votrecode?export&query=1
</code>

<p>Et incrémenter le numéro à la fin pour forcer Google à récupérer à nouveau le nouveau contenu.</p>
</div>

#### Comment puis-je transférer mes données de Framagenda vers une autre instance NextCloud ?

Vous devez télécharger les agendas souhaités en cliquant sur **Télécharger** pour les télécharger sur votre ordinateur&nbsp;:

![agenda télécharger](images/agenda-4.png)

Puis sur le NextCloud sur lequel vous voulez importer vos agendas, vous devez&nbsp;:

* cliquer sur **Paramètres & importation** (tout en bas, à gauche de la page)
* cliquer sur **Importer un agenda**
* sélectionner l'agenda que vous souhaitez importer depuis votre ordinateur
* sélectionner **Nouvel agenda** dans la liste
![image nouvel agenda](images/agenda_faq_import_calendrier.png)
* cliquer sur l'icône <i class="fa fa-check-circle" aria-hidden="true"></i>

#### Les iframes ne fonctionnent pas dans Chrome

Il semble que Chrome rencontre une erreur `ERR_TOO_MANY_REDIRECTS` lorsque l'on intègre un calendrier publié dans une iframe. Vous pouvez suivre l'avancement de la résolution de ce bug [ici](https://github.com/nextcloud/calendar/issues/169).


#### Ajouter une liste Framalistes comme participante à un événement

Framagenda ne peut envoyer des messages à votre liste de diffusion uniquement si n'importe qui peut envoyer des mails à celle-ci (liste de type hotline) ou bien si vous ajoutez l'email **framagenda@framagenda.org** comme abonné à votre liste de diffusion.

Attention, lorsque vous ajoutez cet email, prenez bien soin de cocher la case **Silencieusement** pour ne pas que cet email reçoive de notification d'inscription. En effet, tout email envoyé à l'adresse **framagenda@framagenda.org** apparaîtra comme une erreur dans Framalistes. De même, rendez-vous dans les options de l'abonné et sélectionnez pour **Réception** : **Ne pas recevoir les messages de la liste**.

Ensuite, vous pouvez ajouter l'adresse de la liste Framaliste comme participant sur un événement. Notez toutefois que la fonctionnalité d'invitation n'aura pas de sens ici (c'est l'ensemble de la liste qui est invité, pas ses membres inscrits).

#### Peut-on ajouter un groupe de contacts comme participant à un événement ?

Vous pouvez suivre l'avancement de cette fonctionnalité sur ce [ticket](https://github.com/nextcloud/calendar/issues/167).

#### Pourquoi ai-je plein de personnes inconnues lorsque je veux ajouter un de mes contacts comme participant à un événement ?

Vous pouvez ajouter vos contacts et n'importe quel utilisateur de Framagenda comme participant à un événement. Cela explique que vous voyez probablement plein de noms inconnus pendant l'auto-complétion. Nous avons ouvert [un ticket](https://github.com/nextcloud/calendar/issues/168) pour le développement de cette fonctionnalité.

## Partage et publication

#### Je ne vois pas l'icône de partage à côté d'un agenda

Si vous ne voyez pas l'icône de partage, il est possible qu'elle soit masquée par un bloqueur de pub de type AdBlock, Ghostery ou Disconnect. Il vous faut alors désactiver le bloqueur de pub pour le site framagenda.org (nous n'affichons pas de publicité ni ne traquons nos utilisateurs).

#### Puis-je partager mes agendas avec des utilisateurs de NextCloud ou d'ownCloud qui ne sont pas sur Framagenda ?
Ce n'est actuellement pas possible. Le partage d'agendas entre utilisateurs n'est actuellement possible qu'au sein d'une même instance de NextCloud ou d'ownCloud.

#### Comment puis-je intégrer un calendrier public dans ma page web ?

<div class="alert-warning alert">
La fonctionnalité permettant d’embarquer son agenda sur un site Internet ne fonctionne malheureusement plus. Ce problème est connu de l’équipe qui développe le logiciel que nous utilisons pour Framagenda. Par conséquent, il est possible que vous rencontriez le message suivant :
<blockquote>
  <p>L’agenda n’existe pas</p>
  <p>Vous avez peut-être obtenu un mauvais lien ou le partage de l’agenda a été annulé ?</p>
</blockquote>

ou

<blockquote>
  <p>Bloqué par une stratégie de sécurité de contenu</p>
  <p>Une erreur est survenue pendant une connexion à framagenda.org.</p>
  <p>Firefox a empêché le chargement de cette page de cette manière car sa stratégie de sécurité de contenu ne le permet pas.</p>
</blockquote>

Il n’y a, pour le moment, pas de solution.
(Ticket en anglais en relation avec ce bug : <a href="https://github.com/nextcloud/calendar/issues/169">https://github.com/nextcloud/calendar/issues/169</a>)
</div>

Cliquez sur le lien public de l'agenda. En bas à gauche de la page, cliquez sur **Paramètres**, puis copiez le code en dessous de **Code pour intégrer une iframe**. Collez ce code dans le code html de votre site web à l'endroit où vous voulez positionner l'agenda.

Note : Pour des raisons de sécurité, le système de gestion de contenu Wordpress désactive l'utilisation des iframes. Vous pouvez utiliser un plugin comme [celui-ci](https://fr.wordpress.org/plugins/iframe/) pour résoudre ce problème.

## Clients

#### Je n'arrive pas à me connecter avec mon client CalDAV !

Checklist avant de contacter le support :

* Vous avez bien accès à Internet. Pas de connexion via un proxy qui pourrait poser problème.
* Vous arrivez bien à vous connecter à Framagenda (l'interface web) sur votre appareil&nbsp;?
* Vous mettez bien l'URL de base adéquate pour chaque client (ce n'est pas forcément la même)&nbsp;!
* L'identifiant et le mot de passe sont corrects.
* Vous n'avez pas activé l'authentification en deux étapes dans Framagenda.
* Au moins un agenda est activé sur Framagenda.

#### Pourquoi la synchronisation des agendas doit se faire agenda par agenda sur Thunderbird ?
Thunderbird ne semble hélas pas supporter la découverte de tous les agendas d'un utilisateur comme le font d'autres clients comme DAVx⁵, il est donc nécessaire de récupérer l'adresse de chaque agenda pour le synchroniser.

#### J'ai des soucis de synchronisation avec plusieurs de mes agendas sur Thunderbird !

**Avec Windows**

Vous pouvez essayer cette méthode :

* Dans Thunderbird, menu **Outils | Options**, cliquez sur l'icône **Avancé**
* Onglet **Général** / bouton **Editeur de configuration**
* Chercher *Calendar.network.multirealm* et basculer la valeur à true en double cliquant.
* Redémarrer Thunderbird

Il est possible que les mises à jour de Thunderbird écrasent ce réglage.

**Avec GNU/Linux**

Vous pouvez essayer cette méthode :

* Dans Thunderbird, menu **Édition** / **Préférences** / **Avancé** / puis onglet **Général**
* Zone **Configuration avancée** / bouton **Editeur de configuration**
* Chercher *Calendar.network.multirealm* et basculer la valeur à true en double cliquant.
* Redémarrer Thunderbird

Il est possible que les mises à jour de Thunderbird écrasent ce réglage.

#### L'application OpenTasks ne se synchronise pas avec DAVx⁵ !

Essayez de réinstaller DAVx⁵ et de le reconfigurer, il devrait détecter votre installation d'OpenTasks.

#### Je connais un client fonctionnel qui n'est pas listé dans la liste des clients recommandés !
Merci de nous envoyer un message via https://contact.framasoft.org/ pour nous le signaler.

#### Je n'arrive pas à synchroniser mon Framagenda avec mon téléphone iOS

Si votre identifiant comporte une espace, la synchronisation ne fonctionnera pas en suivant cette [la procédure](Synchronisation/iOS.html). Ceci est dû à un bug entre le logiciel que nous utilisons pour Framagenda et iOS ([voir le ticket Github en anglais](https://github.com/nextcloud/server/issues/6192)).
