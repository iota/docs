# Synchronisation avec Apple Calendrier (iCal)
[<i class="fa fa-arrow-left" aria-hidden="true"></i> Retour à l'accueil](../README.md)

  1. Allez dans les préférences système, puis cliquez sur **Comptes Internet**
  * Cliquez sur **Ajouter un autre compte** puis choisissez **Ajouter un compte CalDAV**.
  * Sélectionnez **Avancé** comme type de compte et entrez les informations relatives à votre compte
    * **Nom de l'utilisateur** : votre identifiant Framagenda ou l'adresse mail associée
    * **Mot de passe** : le mot de passe d'application généré via [vos paramètres > **Sécurité**](https://framagenda.org/settings/user/security)
      <p class="alert alert-warning">Il ne s'agit pas du mot de passe de votre compte mais du <b><a href="../Inscription-Connexion.html#utiliser-les-mots-de-passe-dapplication">mot de passe d'application</a></b> !</p>
    * **Adresse du serveur** : `https://framagenda.org`
    * **Chemin du serveur** : `/remote.php/dav/principals/users/VOUS/`où `VOUS` est à remplacer par votre identifiant
    * **Port** : 443
    * **Utiliser SSL** coché si ce n'est pas le cas
  * Cliquez sur **Se connecter**

Pour CalDAV (Calendrier) : vous pouvez maintenant sélectionner les applications avec lesquelles vous voulez utiliser cette ressource. Dans la plupart des cas, il s'agit de l'application "Calendrier", parfois vous pouvez aussi l'utiliser pour vos Tâches et rappels.

Vos agendas devraient maintenant être ajoutés dans l'application Calendrier de MacOS X.
