<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant une « issue » décrivant les modifications,
soit en proposant une « merge request ».</p>

# Modules

## Calendrier

L'ajout d'un calendrier ne fonctionne qu'avec les agendas [Framagenda](https://framagenda.org/) ou Google Agenda.

### Framagenda

Pour Framagenda, vous devez récupérer [le lien de publication de votre calendrier](https://docs.framasoft.org/fr/agenda/Interface-Agenda.html#publication) de votre calendrier, puis&nbsp;:
  1. sélectionner **Framagenda**
  * coller le lien public de votre calendrier
  * cliquer sur **VALIDER**

![image de la procédure d'ajout de calendrier](images/pnc_module_agenda.png)

## Bouton

Le module **Bouton** est un nodule permettant d'afficher un bouton avec un lien configurable, avec une image de fond optionnelle. Une fois ajouté à votre page vous devez le configurer.

![affichage des options du module bouton](images/pnc-module-bouton-interface.png)

  1. permet de changer l'image de fond&nbsp;; Taille recommandée&nbsp;: 1900x600px
  * permet de modifier le texte au-dessus du bouton
  * permet de modifier le texte et le lien du bouton

Ne pas oublier d'enregistrer&nbsp;: ![bouton enregistrer](images/pnc-enregistrer-changements.png)
