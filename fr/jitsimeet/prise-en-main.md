# Prise en main

## Créer une discussion en 3 étapes

1. Choisissez le nom de vote salon ![nom du salon](images/framatalk01.png)
2. Autorisez l’utilisation de votre micro et de votre caméra ![autorisation micro et caméra](images/framatalk02bis.png)
3. Partagez l’adresse web (URL) pour converser avec vos ami·e·s ![salon Framatalk](images/framatalk04-1.png)

## Utilisation sur mobile

Pour utiliser Framatalk sur mobile vous devez télécharger l'application ([android](http://jitsi.org/android) ou [iOS](http://jitsi.org/ios)).

Ensuite saisissez le nom du salon ou l'adresse complète de votre salon (comme `https://framatalk.org/test-public`)

![image de la saisie d'un salon talk](images/framatalk_saisie_salon.png)

Vous pouvez aussi mettre Framatalk comme serveur par défaut en allant dans vos paramètres <i class="fa fa-bars" aria-hidden="true"></i> puis **Paramètres**&nbsp;:

![image url du serveur application](images/framatalk_appli_url_serveur.png)
