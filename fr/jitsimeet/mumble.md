# Mumble (audio conférence)

Pour des conférences ne nécessitant pas de vidéo vous pouvez utiliser le serveur **Mumble** que nous mettons à disposition. Pour ce faire vous devez utiliser un logiciel sur votre ordinateur ou une application sur votre téléphone.

## Sur ordinateur

### Installation et configuration

Vous devez installer le logiciel **Mumble** téléchargeable sur https://www.mumble.info/downloads/ (disponible sur Windows, Mac OSX et GNU/Linux).

Pour l'installation, les paramètres par défaut devraient suffire sauf pour&nbsp;:

  * la détection de l'activité vocale pour laquelle il est conseillé d'utiliser une touche pour parler
    ![paramètre détection vocale](images/mumble_detection_vocale.png)
  * l'utilisation d'écouteurs pour un meilleur confort
    ![paramètre écouteurs](images/mumble_ecouteurs.png)

<div class="alert-warning alert">Attention : par défaut il est possible que la synthèse vocale soit activée ; pour la désactiver vous devez cliquer sur <b>Configurer</b> et (dé)cocher <b>sythèse vocale</b>. La synthèse vocale énonce vocalement les messages de la messagerie instantanée de Mumble, ce qui peut être perturbant lors de discussions.</div>

Une fois paramétré le logiciel vous devez ajouter le serveur de Framasoft.

Vous pouvez cliquer sur ce lien, <mumble://mumble.framatalk.org?title=framatalk> et choisir le logiciel **Mumble** pour l’ouvrir ou&nbsp;:

  1. cliquer sur **Ajouter nouveau…**
  * entrer le nom d'utilisateur que vous souhaitez afficher sur les salons et cliquer sur **OK**
  * indiquer&nbsp;:
    * **Adresse** : `mumble.framatalk.org` (ou copier `mumble://mumble.framatalk.org?title=framatalk` puis cliquer sur **Fill** pour ajouter l'adresse du serveur dans votre logiciel et lui donner le nom **Frama**)
    * **Port** par défaut
    * **Nom d'utilisateur** que vous souhaitez (si vous souhaitez le changer)
    * **Nom** que vous voulez donner pour le retrouver sans la liste (vous pouvez laisser celui par défaut ou l'appeler `framatalk` ou autre)
  * cliquer sur **Connexion** ou double-cliquer sur le nom du salon dans la liste

![mumble serveur config](images/mumble_serveur.gif)

### Créer un salon sur ordinateur

Une fois sur le serveur mumble de Framatalk vous pouvez créer un salon.

<div class="alert-warning alert">Attention à choisir un nom de salon pas trop générique, sous risque d’attirer des gens qui essayent de rejoindre un salon au nom similaire</div>

Pour ce faire vous devez&nbsp;:

  1. faire un clic droit sur **Root** et cliquer sur **Ajouter…**
  * donner un nom au salon
  * [optionnel] donner une description au salon
  * [optionnel] cocher **Temporaire** pour que le salon soit "détruit" une fois vide
  * cliquer sur **OK**

![paramétrage d'un salon mumble](images/mumble-ajouter-salon.png)

Vous pouvez mettre un mot de passe au salon pour que seules les personnes le possédant puissent entrer. Pour ce faire vous devez&nbsp;:

  1. faire un clic droit sur le nom de votre salon dans la liste des salons
  * cliquer sur **Modifier…**
  * ajouter un mot de passe dans le champ **Mot de passe**
  * cliquer sur **OK**

Vous pouvez copier l'adresse du salon en faisant un clic-droit sur le nom de celui-ci dans la liste puis en cliquant sur **Copier l'URL** : il vous suffit alors de diffuser cette URL parmi vos contacts. Ceux-ci se connecteront directement au salon en cliquant dessus.

### Rejoindre un salon sur ordinateur

Pour rejoindre un salon il vous suffit de double-cliquer sur son nom dans la liste.

Si ce salon est protégé par un mot de passe et que vous tentez de vous y connecter sans vous obtiendrez l'erreur **Permission non accordée !**.

<div class="alert-warning alert">Attention : même le créateur du salon doit ajouter le mot de passe pour pouvoir y parler (même s’il peut s’y connecter sans mot de passe)</div>

Pour y accéder vous devez&nbsp;:

  1. cliquer sur **Serveur** dans la fenêtre listant les salons
  * cliquer sur **Access tokens…**
  * cliquer sur **Ajouter**
  * entrer le mot de passe du salon
  * appuyer sur la touche **Entrée**
  * cliquer sur **OK**
  * rejoindre le salon

### Supprimer un salon sur ordinateur

Pour supprimer un salon que vous avez créé, vous devez&nbsp;:

  1. faire un clic-droit sur le salon
  * cliquer sur **Supprimer…**

## Sur téléphone

Pour utiliser l'audio-conférence sur téléphone vous devez utiliser une application : [Plumble](https://play.google.com/store/apps/details?id=com.morlunk.mumbleclient) sur le PlayStore android, ou [Mumla](https://f-droid.org/fr/packages/se.lublin.mumla/) sur le magasin alternatif F-droid (plus à jour que Plumble), ou encore sur iOS avec [Mumble](https://apps.apple.com/fr/app/mumble/id443472808).

<div class="alert-info alert">Cette documentation a été faite sur l'application Plumble</div>

### Configuration

Une fois installée et lancée, vous devez&nbsp;:

  1. cliquer sur **+** tout en haut
  * ajouter un libellé
  * ajouter l'adresse du serveur : `mumble.framatalk.org`
  * ajouter un nom d'utilisateur (attention : un nom d'utilisateur doit être unique ; si vous rencontrez une erreur vous devez en choisir un autre)
  * cliquer sur **Ajouter**

![conf plumble](images/plumble_conf.jpg)

### Créer un salon sur téléphone

Pour créer un salon vous devez&nbsp;:

  1. cliquer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i> sur la ligne **root**
  * cliquer sur **Ajouter**
  * Donner un nom à votre salon
  * cliquer sur **Ajouter**

![gif création salon plumble](images/plumble_creation_salon.gif)

### Rejoindre un salon sur téléphone

Pour rejoindre un salon vous devez&nbsp;:

  1. appuyer sur le serveur de Framasoft dans la liste
  * appuyer sur la flèche sur la ligne du salon que vous souhaitez rejoindre

![image rejoindre salon plumble](images/plumble-rejoindre-salon.jpg)

Pour rejoindre un salon **avec mot de passe** vous devez entrer ce mot de passe. Pour ce faire vous devez&nbsp;:

  1. appuyer sur <i class="fa fa-bars" aria-hidden="true"></i>
  * appuyer sur **Jetons d'accès**
  * entrer le mot de passe dans le champ **Ajouter un jeton d'accès** tout en bas
  * appuyer sur **+**

### Suppression d'un salon sur téléphone

Pour supprimer un salon que vous avez créé, vous devez&nbsp;:

  1. appuyer sur l'icône <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
  * appuyer sur **Supprimer**
  * confirmer la suppression

### Déconnexion sur téléphone

Pour vous déconnecter vous devez&nbsp;:

  1. appuyer sur <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
  * appuyer sur **Déconnecter**

### Désactiver la synthèse vocale du chat

Par défaut la synthèse vocale est activée pour l'application. Cela signifie que le texte écrit dans le chat sera lu par une synthèse vocale. Pour la désactiver, vous devez&nbsp;:

  1. cliquer sur <i class="fa fa-bars" aria-hidden="true"></i>
  * cliquer sur **Paramètres**
  * cliquer sur **Général**
  * décocher **Synthèse vocale**

### Astuces

  * Pour éviter les échos, il est préférable d'utiliser le push-to-talk, c'est à dire maintenir appuyé le bouton **APPUYER POUR PARLER** tout en bas quand on veut parler. Pour cela :
      1. appuyez sur le menu (les trois barres horizontales en haut à gauche)
      * **Paramètres**
      * **Audio**
      * cochez la case **Activer le mode “Appuyer pour parler”**
  * Pour garder l'écran allumé quand Plumble est activé (pratique avec le push-to-talk) :
      1. appuyez sur le menu (les trois barres horizontales en haut à gauche)
      * **Paramètres**
      * **Général**
      * cochez **Rester activé**

## web

En plus de pouvoir utiliser un logiciel sur l'ordinateur ou sur téléphone, vous pouvez aussi utiliser mumble depuis votre navigateur.

Avant tout, veuillez noter que&nbsp;:

  * il n'est pas possible de **créer** de salon depuis cette interface web : vous devez passer par le logiciel ordinateur ou l'application (voir ci-dessus)
  * il faut rafraîchir la page si vous créez le salon après vous être connecté sur le navigateur


Pour ce faire vous devez&nbsp;:

  1. vous rendre sur https://web.mumble.framatalk.org/
  * autoriser l'utilisation de votre micro

   ![image montrant la demande d'autorisation d'utiliser le micro](images/mumble-web-mico-droits.png)
  * indiquer le pseudo que vous souhaitez utiliser dans le champ **Nom d'utilisateur** (et laisser le reste par défaut)

   ![image pseudo](images/mumble-web-connect-server.png)
  * cliquer sur **Connect**
  * rejoindre un salon en double-cliquant sur son nom ou en faisant un clic droit dans la liste puis **Rejoindre le canal**

### Changer les touches permettant de prendre la parole

<div class="alert-warning alert">
Par défaut, vous devez utiliser la combinaison de touches <b>ctrl</b> et <b>maj</b> pour pouvoir parler.
</div>

Pour changer la touche permettant de parler vous devez&nbsp;:

  1. cliquer sur l'icône <i class="fa fa-cog" aria-hidden="true"></i> dans le menu
  * cliquer sur le bouton `ctrl + shift` puis choisir une nouvelle touche (le `²` par exemple si vous souhaitez utiliser votre clavier en plus de discuter)
  * cliquer sur **Apply**

![image des paramètres mumble](images/mumble-web-settings.png)

### Changer de pseudo

Pour changer de pseudo vous devez&nbsp;:

  1. cliquer sur ![icone terre](images/mumble-web-terre.png)
  * modifier le pseudo dans le champ **Nom d'utilisateur**
  * cliquer sur **Connect** pour relancer la page


### Entrer dans un salon protégé par un mot de passe

**Rappel**&nbsp;: il n'est pas possible de **créer** un salon (avec ou sans mot de passe) sur la version web.

Pour **rejoindre** un salon protégé par un mot de passe vous devez indiquer ce mot de passe dans le champ **Jeton** sur l'écran de connexion :

![écran de connexion mumble web](images/mumble-web-connect-server.png)
